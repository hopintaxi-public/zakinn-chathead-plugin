package com.hopinchathead;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.WindowManager;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

public class HopinChathead extends CordovaPlugin {

    private CallbackContext callbackContext = null;

    @Override
    public void onResume(boolean multitasking) {
        Log.d("HOPINCHATHEAD", "onResume");
    }

    @Override
    public void onPause(boolean multitasking) {
        Log.d("HOPINCHATHEAD", "onPause");
    }

    @Override
    public void onDestroy() {
        Log.d("HOPINCHATHEAD", "onDestroy");
    }

    private void broadcastBackgroundState(boolean isBackground) {
        final Intent intent = new Intent(ChatHeadService.MAIN_APP_STATE_CHANGE_EVENT);
        Bundle b = new Bundle();
        b.putBoolean("isBackground", isBackground);
        intent.putExtras(b);
        LocalBroadcastManager.getInstance(super.webView.getContext()).sendBroadcastSync(intent);
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        webView.getView().setFilterTouchesWhenObscured(true);
        if (action.equals("init")) {
            callbackContext.sendPluginResult(this.activate());
            return true;
        } else if (action.equals("show")) {
            callbackContext.sendPluginResult(this.show());
            return true;
        } else if (action.equals("hide")) {
            callbackContext.sendPluginResult(this.hide());
            return true;
        } else if (action.equals("destroy")) {
            callbackContext.sendPluginResult(this.destroy());
            return true;
        } else if (action.equals("hasPermission")) {
            callbackContext.success(this.hasPermission());
            return true;
        } else if (action.equals("requestPermission")) {
            HopinChathead.this.callbackContext = callbackContext;
            this.requestPermission();
            return true;
        } else if (action.equals("moveTaskToBack")) {
            callbackContext.sendPluginResult(this.moveTaskToBack());
        } else if (action.equals("enableOverLockScreen")) {
            callbackContext.sendPluginResult(this.enableOverLockScreen());
            return true;
        } else if (action.equals("disableOverLockScreen")) {
            callbackContext.sendPluginResult(this.disableOverLockScreen());
            return true;
        }
        else if (action.equals("bringToFront"))
        {
            callbackContext.sendPluginResult(this.bringToFront());
            return true;
        }
        else if (action.equals("bringToFront2"))
        {
            callbackContext.sendPluginResult(this.bringToFront2());
            return true;
        }

        return false;
    }

    private PluginResult enableOverLockScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    cordova.getActivity().setShowWhenLocked(true);
                    cordova.getActivity().setTurnScreenOn(true);

                    KeyguardManager keyguardManager = (KeyguardManager) cordova.getActivity().getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
                    keyguardManager.requestDismissKeyguard(cordova.getActivity(), null);

                }
            });
        } else {
            cordova.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    cordova.getActivity().getWindow().addFlags(
                            WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
                }
            });
        }

        return new PluginResult(PluginResult.Status.OK, true);
    }

    private PluginResult disableOverLockScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    cordova.getActivity().setShowWhenLocked(false);
                    cordova.getActivity().setTurnScreenOn(false);

                    KeyguardManager keyguardManager = (KeyguardManager) cordova.getActivity().getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
                    keyguardManager.requestDismissKeyguard(cordova.getActivity(), null);
                }
            });
        } else {
            cordova.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    cordova.getActivity().getWindow().clearFlags(
                            WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
                }
            });
        }

        return new PluginResult(PluginResult.Status.OK, true);
    }

    private PluginResult moveTaskToBack() {
        if (this.cordova.getActivity().moveTaskToBack(true)) {
            return new PluginResult(PluginResult.Status.OK, true);
        } else {
            return new PluginResult(PluginResult.Status.OK, false);
        }
    }

    private int hasPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return 1;
        } else {
            return Settings.canDrawOverlays(this.cordova.getContext()) ? 1 : 0;
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this.cordova.getContext())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + this.cordova.getActivity().getPackageName()));
                // specify that this plugin will handle activity result 'onActivityResult'
                this.cordova.setActivityResultCallback(this);

                this.cordova.getActivity().startActivityForResult(intent, REQUEST_PERMISSION_CODE);
            }
        } else {
            this.callbackContext.success(hasPermission());

            this.callbackContext = null;
        }
    }

    private PluginResult activate() {
        cordova.getThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                Log.d("HOPINCHATHEAD", "activate");
                if (!ChatHeadService.SERVICE_STARTED) {
                    Log.d("HOPINCHATHEAD", "start service");
                    Context context = cordova.getActivity();
                    context.startService(new Intent(context, ChatHeadService.class));
                }
            }
        });

        return new PluginResult(PluginResult.Status.OK, "SERVICE STARTED");
    }

    private PluginResult show() {
        broadcastBackgroundState(true);

        return new PluginResult(PluginResult.Status.OK, "shown");
    }

    private PluginResult hide() {
        broadcastBackgroundState(false);

        return new PluginResult(PluginResult.Status.OK, "hidden");
    }

    private PluginResult destroy() {
        cordova.getThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                Log.d("HOPINCHATHEAD", "destroy chathead");
                Context context = cordova.getActivity().getApplicationContext();
                context.stopService(new Intent(context, ChatHeadService.class));
            }
        });

        return new PluginResult(PluginResult.Status.OK, "SERVICE DESTROYED");
    }

    private PluginResult bringToFront()
    {
        Context context = cordova.getActivity().getApplicationContext();
        context.startService(new Intent(context, ChatHeadService.class));

        return new PluginResult(PluginResult.Status.OK, true);
    }

    private PluginResult bringToFront2()
    {
        Context context = this.cordova.getActivity().getApplicationContext();
        PackageManager pm = context.getPackageManager();

        Intent startAppIntent = pm.getLaunchIntentForPackage(context.getPackageName());

        context.startActivity(startAppIntent);

        return new PluginResult(PluginResult.Status.OK, true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PERMISSION_CODE) {
            this.callbackContext.success(hasPermission());

            this.callbackContext = null;
        }
    }

    private int REQUEST_PERMISSION_CODE = 5469;
    // before 2 changed to 5469 to try to fix permission plugin
}
